package com.william.restws.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.william.restws.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

}
